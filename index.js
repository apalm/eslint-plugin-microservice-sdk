const message = "Service call is missing `valid`";

module.exports = {
  rules: {
    "no-missing-valid": {
      create(context) {
        return {
          CallExpression(node) {
            if (
              node.callee.type === "MemberExpression" &&
              node.callee.object.type === "Identifier" &&
              node.callee.object.name.endsWith("Service") &&
              node.callee.property.type === "Identifier" &&
              node.callee.property.name.startsWith("get") &&
              node.callee.property.name.endsWith("List") &&
              node.arguments[0] &&
              node.arguments[0].type === "ObjectExpression"
            ) {
              const objectExpressionWhereProp = node.arguments[0].properties.find(
                x =>
                  x.type === "Property" &&
                  x.key.name === "$where" &&
                  x.value.type === "ObjectExpression"
              );
              if (!objectExpressionWhereProp) {
                return;
              }
              reportErrors(
                objectExpressionWhereProp.value.properties,
                node,
                context
              );
            }
          },
          VariableDeclarator(node) {
            if (
              node.id.type === "Identifier" &&
              node.id.name === "$where" &&
              node.init.type === "ObjectExpression"
            ) {
              reportErrors(node.init.properties, node, context);
            }
          }
        };
      }
    }
  }
};

function reportErrors(properties, node, context) {
  if (properties.some(x => x.type === "Property" && x.key.name === "valid")) {
    return;
  }

  const andOrProperties = properties.filter(
    x =>
      x.type === "Property" &&
      ["$and", "$or"].includes(x.key.name) &&
      x.value.type === "ArrayExpression"
  );

  if (andOrProperties.length > 0) {
    for (let ys of andOrProperties) {
      for (let zs of ys.value.elements.filter(
        z => z.type === "ObjectExpression"
      )) {
        reportErrors(zs.properties, node, context);
      }
    }
  } else {
    context.report({ node, message });
  }
}

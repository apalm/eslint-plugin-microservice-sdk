const { RuleTester } = require("eslint");
const { rules } = require("../index");

const ruleTester = new RuleTester({
  parserOptions: { ecmaVersion: 2018 }
});

ruleTester.run("no-missing-valid", rules["no-missing-valid"], {
  valid: [
    {
      code: `teamService.getTeamUserList(
        { $where: { userUuid: req.user.uuid, valid: true } },
        { correlationId }
      );`
    },
    {
      code: `teamService.getTeamUserList(
        { $where: { userUuid: req.user.uuid, valid: false } },
        { correlationId }
      );`
    },
    // {
    //   code: `teamService.deleteTeamUserList(
    //     { $where: { userUuid: req.user.uuid, valid: true } },
    //     { correlationId }
    //   );`
    // },
    {
      // Should not have errors on callee object name that doesn't meet criteria
      code: `foo.getTeamUserList(
        { $where: { userUuid: req.user.uuid } },
        { correlationId }
      );`
    },
    {
      // Should not have errors on callee property name that doesn't meet criteria
      code: `teamService.foo(
        { $where: { userUuid: req.user.uuid } },
        { correlationId }
      );`
    },
    {
      code: `const $where = {
        teamUuid: req.team.uuid,
        teamEventTypeUuid: teamEventTypeUuidQuery,
        enum: req.query.enum === undefined ? undefined : req.query.enum,
        name: req.query.name === undefined ? undefined : req.query.name,
        valid: true
      };`
    },
    {
      code: `teamService.getConsumerDeviceList(
        {
          $where: {
            $or: [
              {
                consumerUuid,
                valid: true,
                expires: { $gt: moment().unix() }
              },
              {
                consumerUuid,
                valid: true,
                expires: null
              }
            ]
          }
        },
        { correlationId }
      );`
    },
    {
      code: `teamService.getConsumerDeviceList(
        {
          $where: {
            valid: true,
            $or: [
              {
                consumerUuid,
                expires: { $gt: moment().unix() }
              },
              {
                consumerUuid,
                expires: null
              }
            ]
          }
        },
        { correlationId }
      );`
    }
  ],
  invalid: [
    {
      code: `teamService.getTeamUserList(
        { $where: { userUuid: req.user.uuid } },
        { correlationId }
      );`,
      errors: [{ message: "Service call is missing `valid`" }]
    },
    // {
    //   code: `teamService.deleteTeamUserList(
    //       { $where: { userUuid: req.user.uuid } },
    //       { correlationId }
    //     );`,
    //   errors: [{ message: "Service call is missing `valid`" }]
    // },
    {
      code: `const $where = {
        teamUuid: req.team.uuid,
        teamEventTypeUuid: teamEventTypeUuidQuery,
        enum: req.query.enum === undefined ? undefined : req.query.enum,
        name: req.query.name === undefined ? undefined : req.query.name,
      };`,
      errors: [{ message: "Service call is missing `valid`" }]
    },
    {
      code: `teamService.getConsumerDeviceList(
        {
          $where: {
            $or: [
              {
                consumerUuid,
                valid: true,
                expires: { $gt: moment().unix() }
              },
              {
                consumerUuid,
                expires: null
              }
            ]
          }
        },
        { correlationId }
      );`,
      errors: [{ message: "Service call is missing `valid`" }]
    }
  ]
});
